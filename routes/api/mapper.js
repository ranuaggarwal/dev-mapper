const express = require("express");
const router = express.Router();

const Maps = require("../../models/Mapper");

// @route Get api/books
// @des
//
router.get("/", async (req, res) => {
  try {
    const mapper = await Mapper.find();
    res.json(mapper);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("server error");
  }
});

router.post("/add", async (req, res) => {
  const section = req.body.section;
  const question = req.body.question;
  const maprDetail = { section, question };

  try {
    let map = await Mapper.findOne({ section: req.body.section });
    if (map) {
      map = await Mapper.findOneAndUpdate(
        { section: req.body.section },
        { $set: maprDetail },
        { new: true }
      );
      return res.json(map);
    }

    const newMap = new Mapper({ section, question });
    await newMap.save();
    res.json(newMap);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("server error");
  }
});

module.exports = router;

// {

//     "name": "book1",
//     "chapter": [
//         {

//             "name": "Chapter One",
//             "section": [
//                 {
//                     "question": [
//                         "5e18699c0e727c0ad0fc93d7",
//                         "5e1869aa0e727c0ad0fc93d8"
//                     ],

//                     "name": "sec 1.1"
//                 }
//             ]
//         }
//     ],
//     "__v": 0
// }

// {
//     "_id": "5e1869aa0e727c0ad0fc93d8",
//     "name": "Q2",
//     "type": "MCQ",
//     "desc": "What is your age",
//     "url": "hi",
//     "__v": 0
// }
