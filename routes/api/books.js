const express = require("express");
const router = express.Router();

const Book = require("../../models/Book");
const Question = require("../../models/Question");

// @route Get api/books
// @des
//
router.get("/", async (req, res) => {
  try {
    const books = await Book.find().populate("chapter.section.question", [
      "name",
      "type",
      "desc",
      "url"
    ]);
    res.json(books);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("server error");
  }
});

router.get("/booklist", async (req, res) => {
  try {
    const books = await Book.find({}).select("name");
    res.json(books);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("server error");
  }
});

router.get("/:id", async (req, res) => {
  try {
    let params = req.params;
    const books = await Book.findById(params.id);
    res.json(books);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("server error");
  }
});

router.post("/add", async (req, res) => {
  const name = req.body.name;
  const chapter = req.body.chapter;
  const newBook = new Book({ name, chapter });

  try {
    await newBook.save();
    res.json(newBook);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("server error");
  }
});

module.exports = router;

// {

//     "name": "book1",
//     "chapter": [
//         {

//             "name": "Chapter One",
//             "section": [
//                 {
//                     "question": [
//                         "5e18699c0e727c0ad0fc93d7",
//                         "5e1869aa0e727c0ad0fc93d8"
//                     ],

//                     "name": "sec 1.1"
//                 }
//             ]
//         }
//     ],
//     "__v": 0
// }

// {
//     "_id": "5e1869aa0e727c0ad0fc93d8",
//     "name": "Q2",
//     "type": "MCQ",
//     "desc": "What is your age",
//     "url": "hi",
//     "__v": 0
// }
