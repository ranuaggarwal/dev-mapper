const express = require("express");
const router = express.Router();

const Question = require("../../models/Question");

router.get("/", async (req, res) => {
  try {
    const questions = await Question.find();
    res.json(questions);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("server error");
  }
});

router.post("/add", async (req, res) => {
  const name = req.body.name;
  const type = req.body.type;
  const desc = req.body.desc;
  const url = req.body.url;

  const newQuestion = new Question({ name, type, desc, url });

  try {
    await newQuestion.save();
    res.json(newQuestion);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("server error");
  }
});

module.exports = router;
