const express = require("express");
const router = express.Router();

const BookTest = require("../../models/BookText");

// @route Get api/books
// @des
//
router.get("/", async (req, res) => {
  try {
    const books = await BookTest.find();
    res.json(books);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("server error");
  }
});

router.get("/booklist", async (req, res) => {
  try {
    const books = await BookTest.find({}).select("name");
    res.json(books);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("server error");
  }
});

router.get("/:id", async (req, res) => {
  try {
    let params = req.params;
    const books = await BookTest.findById(params.id);
    res.json(books);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("server error");
  }
});

router.post("/add", async (req, res) => {
  const name = req.body.name;
  const chapter = req.body.chapter;
  console.log(req.body.chapter);
  const newBook = new BookTest({ name, chapter });

  try {
    await newBook.save();
    res.json(newBook);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("server error");
  }
});

module.exports = router;

// {

//     "name": "book1",
//     "chapter": [
//         {

//             "name": "Chapter One",
//             "section": [
//                 {
//                     "question": [
//                         "5e18699c0e727c0ad0fc93d7",
//                         "5e1869aa0e727c0ad0fc93d8"
//                     ],

//                     "name": "sec 1.1"
//                 }
//             ]
//         }
//     ],
//     "__v": 0
// }

// {
//     "_id": "5e1869aa0e727c0ad0fc93d8",
//     "name": "Q2",
//     "type": "MCQ",
//     "desc": "What is your age",
//     "url": "hi",
//     "__v": 0
// }
