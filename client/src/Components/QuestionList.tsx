import React from "react";
import { Store } from "../Store";
import { IQuestion } from "../Data/Data";
import { Question } from "./Question";

export const QuestionList: React.FC = () => {
  const { state, dispatch } = React.useContext(Store);

  React.useEffect(() => {
    state.questionList.length === 0 && fetchDataAction();
  });

  const fetchDataAction = async () => {
    const URL = "/api/question";
    const data = await fetch(URL);
    const dataJSON = await data.json();
    return dispatch({
      type: "FETCH_QUESTIONS",
      payload: dataJSON
    });
  };

  const updateState = (value: IQuestion) => {
    const questionIsSelected = state.selectedQuestionList.includes(value);
    if (!questionIsSelected) {
      return dispatch({
        type: "ADD_QUESTIONS",
        payload: value
      });
    }
  };

  return (
    <React.Fragment>
      <div className="MyInput">Question List</div>
      {state.questionList.map((question: IQuestion) => (
        <Question
          key={question._id}
          queContent={question}
          updateState={updateState}
        ></Question>
      ))}
    </React.Fragment>
  );
};

export default QuestionList;
