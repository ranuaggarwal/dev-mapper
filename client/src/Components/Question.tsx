import React from "react";
import Paper from "@material-ui/core/Paper";
import { Grid, Button } from "@material-ui/core";
import {
  AddBox,
  Visibility,
  CheckCircleOutline,
  GridOn,
  Equalizer
} from "@material-ui/icons";
import { IQuestion } from "../Data/Data";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      border: "2px solid #000",
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3)
    }
  })
);

function getIconAndText(type: string) {
  if (type === "MCQ") {
    return (
      <>
        <CheckCircleOutline
          style={{ position: "relative", top: "7px", color: "green" }}
        ></CheckCircleOutline>
        &nbsp; MCQ
      </>
    );
  } else if (type === "chart") {
    return (
      <>
        <Equalizer
          style={{ position: "relative", top: "7px", color: "orange" }}
        ></Equalizer>
        &nbsp; Chart
      </>
    );
  } else {
    return (
      <>
        <GridOn
          style={{ position: "relative", top: "7px", color: "darkgreen" }}
        ></GridOn>
        &nbsp; Excel
      </>
    );
  }
}

interface Props {
  key: string;
  queContent: IQuestion;
  updateState(value: IQuestion): void;
}

export const Question: React.FC<Props> = Props => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const updateQuestion = () => {
    Props.updateState(Props.queContent);
  };

  return (
    <>
      <Paper elevation={3} style={{ marginTop: "15px", paddingLeft: "10px" }}>
        <p className="QuestionPara">
          <b>Question: </b>
          {Props.queContent.name}.
        </p>
        <p className="QuestionPara">
          <b>Dexription: </b>
          {Props.queContent.desc}.
        </p>
        <Grid container style={{ textAlign: "center", fontSize: "14px" }}>
          <Grid item xs={4}>
            <Button variant="outlined" color="primary" onClick={updateQuestion}>
              <AddBox></AddBox> &nbsp; Add
            </Button>
          </Grid>
          <Grid item xs={4}>
            <Button variant="outlined" color="primary" onClick={handleOpen}>
              <Visibility></Visibility> &nbsp; View
            </Button>
            <Modal
              aria-labelledby="transition-modal-title"
              aria-describedby="transition-modal-description"
              className={classes.modal}
              open={open}
              onClose={handleClose}
              closeAfterTransition
              BackdropComponent={Backdrop}
              BackdropProps={{
                timeout: 500
              }}
            >
              <Fade in={open}>
                <div className={classes.paper}>
                  {Props.queContent.url !== "" ? (
                    <div
                      dangerouslySetInnerHTML={{
                        __html: Props.queContent.url ? Props.queContent.url : ""
                      }}
                    />
                  ) : (
                    <div>{Props.queContent.desc}</div>
                  )}
                </div>
              </Fade>
            </Modal>
          </Grid>
          <Grid item xs={4}>
            <p
              style={{
                marginTop: "0",
                textTransform: "uppercase",
                fontSize: "15px"
              }}
            >
              {getIconAndText(Props.queContent.type)}
            </p>
          </Grid>
        </Grid>
      </Paper>
    </>
  );
};

export default Question;
