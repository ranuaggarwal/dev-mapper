import React from "react";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { Store } from "../Store";
import { IBookList } from "../Data/Data";

export const MySelector: React.FC = () => {
  const { state, dispatch } = React.useContext(Store);
  const [selectValue, setSelectValue] = React.useState("");

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    let val: string = event.target.value as string;
    setSelectValue(val);
    fetchBookData(val);
    // return dispatch({
    //   type: "UPDATE_SOURCE",
    //   payload: event.target.value as string
    // });
  };

  React.useEffect(() => {
    state.bookList.length === 0 && fetchDataAction();
  });

  const fetchBookData = async (id: string) => {
    console.log(id);
    const URL = "/api/book/" + id;
    const data = await fetch(URL);
    const dataJSON = await data.json();
    return dispatch({
      type: "FETCH_BOOKDATA",
      payload: dataJSON
    });
  };

  const fetchDataAction = async () => {
    const URL = "/api/book/booklist";
    const data = await fetch(URL);
    const dataJSON = await data.json();
    return dispatch({
      type: "FETCH_DATA",
      payload: dataJSON
    });
  };

  const bookItems = state.bookList.map((book: IBookList) => (
    <MenuItem key={book._id} value={book._id}>
      {book.name}
    </MenuItem>
  ));

  return (
    <React.Fragment>
      <div className="MyInput">Select Book To Map</div>
      {/* <InputLabel className="MyInput" id="demo-simple-select-helper-label">
        Select Book To Map
      </InputLabel> */}
      <Select
        name="booklist"
        labelId="demo-simple-select-helper-label"
        id="demo-simple-select-helper"
        value={selectValue}
        onChange={handleChange}
        style={{ width: "100%" }}
      >
        <MenuItem value="">
          <em>None</em>
        </MenuItem>
        {bookItems}
      </Select>
    </React.Fragment>
  );
};

export default MySelector;
