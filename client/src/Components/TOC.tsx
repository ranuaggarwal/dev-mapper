import React from "react";
import { Store } from "../Store";
import TreeView from "@material-ui/lab/TreeView";
import TreeItem from "@material-ui/lab/TreeItem";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { Chapter, Section } from "../Data/Data";

export const TOC: React.FC = () => {
  const { state, dispatch } = React.useContext(Store);

  // React.useEffect(() => {
  //   state.selectedSource !== "" && fetchDataAction();
  // });

  // const fetchDataAction = async () => {
  //   const URL = "/api/book/" + state.selectedSource;
  //   const data = await fetch(URL);
  //   const dataJSON = await data.json();

  //   return dispatch({
  //     type: "FETCH_BOOKDATA",
  //     payload: dataJSON
  //   });
  // };

  const handleEvent = (event: React.MouseEvent<HTMLButtonElement>) => {
    return dispatch({
      type: "SEL_SECTION",
      payload: event.currentTarget.title
    });
  };

  const chapterItems = state.book.chapter.map((item: Chapter) => (
    <TreeItem nodeId={item._id} key={item._id} label={item.name}>
      {item.section.map((sec: Section) => (
        <TreeItem
          nodeId={sec._id}
          key={sec._id}
          label={
            <button title={sec._id} className="MyButton" onClick={handleEvent}>
              {sec.name}
            </button>
          }
        ></TreeItem>
      ))}
    </TreeItem>
  ));

  return (
    <div>
      <TreeView
        //className={classes.root}
        defaultCollapseIcon={<ExpandMoreIcon />}
        defaultExpandIcon={<ChevronRightIcon />}
      >
        {chapterItems}
      </TreeView>
    </div>
  );
};
export default TOC;
