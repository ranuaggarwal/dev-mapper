import React from "react";
import { Store } from "../Store";
import { IQuestion } from "../Data/Data";
import { SelectedQuestion } from "./SelectedQuestion";

export const SelectedQuestionList: React.FC = () => {
  const { state, dispatch } = React.useContext(Store);

  const updateState = (value: IQuestion) => {
    const selQuestionList = state.selectedQuestionList.filter(
      (item: IQuestion) => item._id !== value._id
    );
    return dispatch({
      type: "DELETE_QUESTION",
      payload: selQuestionList
    });
  };

  return (
    <React.Fragment>
      <div className="MyInput">Selected Question List</div>
      {state.selectedQuestionList.map((question: IQuestion) => (
        <SelectedQuestion
          key={question._id}
          queContent={question}
          updateState={updateState}
        ></SelectedQuestion>
      ))}
    </React.Fragment>
  );
};

export default SelectedQuestionList;
