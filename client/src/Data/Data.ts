export interface IQuestion {
  _id: string;
  name: string;
  type: string;
  desc: string;
  url: string;
}

export interface Section {
  _id: string;
  name: string;
  question?: IQuestion[];
}

export interface Chapter {
  _id: string;
  name: string;
  section: Section[];
}

export interface IBookList {
  _id: string;
  name: string;
}

export interface IBook {
  _id: string;
  name: string;
  chapter: Chapter[];
}
