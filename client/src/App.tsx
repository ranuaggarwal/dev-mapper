import React from "react";
import "./App.css";
import { MySelector } from "./Components/MySelector";
import { TOC } from "./Components/TOC";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import AppBar from "@material-ui/core/AppBar";
import { QuestionList } from "./Components/QuestionList";
import { SelectedQuestionList } from "./Components/SelectedQuestionList";

//import TOC from "./Components/TOC";

const App: React.FC = () => {
  //const { state, dispatch } = React.useContext(Store);

  return (
    <div className="App">
      <AppBar
        position="static"
        style={{
          alignItems: "center",
          padding: "10px"
        }}
      >
        <div>Question Mapper</div>
      </AppBar>
      <Grid container style={{ minHeight: "100vh", textAlign: "left" }}>
        <Grid item xs={4}>
          <Paper style={{ height: "100%" }}>
            <MySelector />
            <TOC />
          </Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper style={{ height: "100%" }}>
            <SelectedQuestionList />
          </Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper style={{ height: "100%" }}>
            <QuestionList />
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default App;

