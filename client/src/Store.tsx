import React from "react";
import { IBook, IBookList, IQuestion } from "./Data/Data";

interface IState {
  bookList: IBookList[];
  book: IBook;
  questionList: IQuestion[];
  selectedQuestionList: IQuestion[];
  selectedSection: string;
}

interface IAction {
  type: String;
  payload: any;
}

const initialState: IState = {
  bookList: [],
  book: { _id: "", name: "", chapter: [] },
  questionList: [],
  selectedQuestionList: [],
  selectedSection: ""
};
export const Store = React.createContext<IState | any>(initialState);

function reducer(state: IState, action: IAction): IState {
  switch (action.type) {
    case "FETCH_DATA":
      return { ...state, bookList: action.payload };
    case "FETCH_BOOKDATA":
      return { ...state, book: action.payload };
    case "FETCH_QUESTIONS":
      return { ...state, questionList: action.payload };
    case "ADD_QUESTIONS":
      return {
        ...state,
        selectedQuestionList: [...state.selectedQuestionList, action.payload]
      };
    case "DELETE_QUESTION":
      return { ...state, selectedQuestionList: action.payload };
    case "SEL_SECTION":
      return { ...state, selectedSection: action.payload };
    default:
      return state;
  }
}

export const StoreProvider: React.FC = props => {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  return (
    <Store.Provider value={{ state, dispatch }}>
      {props.children}
    </Store.Provider>
  );
};
