const mongoose = require("mongoose");

const SectionSchema = new mongoose.Schema({ name: String });

const BookTestSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  chapter: [
    {
      name: String,
      section: [SectionSchema]
    }
  ]
});

//module.exports = Section = mongoose.model("Section", SectionSchema);
module.exports = BookTest = mongoose.model("BookTest", BookTestSchema);
