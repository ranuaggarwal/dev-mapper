const mongoose = require("mongoose");

const QuestionSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  type: String,
  desc: String,
  url: String
});

module.exports = Question = mongoose.model("Question", QuestionSchema);
