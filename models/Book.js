const mongoose = require("mongoose");

const BookSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  chapter: [
    {
      name: String,
      section: [
        {
          name: String,
          question: [{ type: mongoose.Schema.Types.ObjectId, ref: "Question" }]
        }
      ]
    }
  ]
});

module.exports = Book = mongoose.model("Book", BookSchema);
