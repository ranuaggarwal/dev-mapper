const mongoose = require("mongoose");

const MapperSchema = new mongoose.Schema({
  section: { type: mongoose.Schema.Types.ObjectId, ref: "Section" },
  question: [{ type: mongoose.Schema.Types.ObjectId, ref: "Question" }]
});

module.exports = Mapper = mongoose.model("Mapper", MapperSchema);
