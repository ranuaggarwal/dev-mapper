const express = require("express");
const connectDB = require("./config/db");
var bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
connectDB();

app.get("/", (req, res) => res.send("API running"));
//app.use(cors());
//Define Routes
var extRouteBook = require("./routes/api/books");
app.use("/api/book", extRouteBook);

var extRouteQue = require("./routes/api/questions");
app.use("/api/question", extRouteQue);

var extRouteBookTest = require("./routes/api/bookTest");
app.use("/api/bookTest", extRouteBookTest);

var extRouteMap = require("./routes/api/mapper");
app.use("/api/mapper", extRouteMap);

const PORT = process.env.Port || 5000;

app.listen(PORT, () => console.log("server started"));
